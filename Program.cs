using EnsekTest.Database;
using EnsekTest.SwaggerFilters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<EnsekDbContext>(opt => opt.UseInMemoryDatabase("CustomerData"));
builder.Services.AddScoped<IEnsekDbContext>(provider => provider.GetService<EnsekDbContext>());
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo{ Title = "ENSEK Coding Test", Version = "v1"});
    c.OperationFilter<SwaggerFileOperationFilter>();
});
builder.Services.AddAsyncInitializer<DatabaseSeeder>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseFileServer(new FileServerOptions  
{  
    FileProvider = new PhysicalFileProvider(  
        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),  
    RequestPath = "",  
    EnableDefaultFiles = true  
}) ;  
app.UseAuthorization();

app.MapControllers();
await app.InitAsync();
await app.RunAsync();