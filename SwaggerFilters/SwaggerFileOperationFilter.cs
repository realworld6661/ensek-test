using System.Reflection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EnsekTest.SwaggerFilters;

/// <summary>
/// Display a file upload field in the Swagger docs
/// Source: https://dejanstojanovic.net/aspnet/2021/april/handling-file-upload-in-aspnet-core-5-with-swagger-ui/
/// </summary>
public class SwaggerFileOperationFilter : IOperationFilter  
{  
    public void Apply(OpenApiOperation operation, OperationFilterContext context)  
    {  
        var fileUploadMime = "multipart/form-data";  
        if (operation.RequestBody == null || !operation.RequestBody.Content.Any(x => x.Key.Equals(fileUploadMime, StringComparison.InvariantCultureIgnoreCase)))  
            return;  
  
        var fileParams = context.MethodInfo.GetParameters().Where(p => p.ParameterType == typeof(IFormFile));
        Dictionary<string, OpenApiSchema> dictionary = new Dictionary<string, OpenApiSchema>();
        foreach (ParameterInfo param in fileParams) dictionary.Add(param.Name, new OpenApiSchema() { Type = "string", Format = "binary" });
        operation.RequestBody.Content[fileUploadMime].Schema.Properties =  
            dictionary;  
    }  
}