using System.ComponentModel.DataAnnotations;

namespace EnsekTest.Models;

public class CustomerAccount
{
    /// <summary>
    /// The account id. Must be unique
    /// </summary>
    [Key]
    public int AccountId { get; set; }

    public string FirstName { get; set; } = "";
    public string LastName { get; set; } = "";
}