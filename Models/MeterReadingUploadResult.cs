namespace EnsekTest.Models;

public class MeterReadingUploadResult
{
    public bool Success { get; set; } = true;
    public string? Error { get; set; }
}

public class MeterReadingUploadResults
{
    public List<MeterReadingUploadResult> Results { get; set; }= new List<MeterReadingUploadResult>();
    public int SuccessfulUploads { get; set; } 
}