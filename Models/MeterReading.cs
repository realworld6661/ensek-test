using System.ComponentModel.DataAnnotations;

namespace EnsekTest.Models;

public class MeterReading
{
    [Key]
    public Guid MeterReadingKey { get; set; }
    
    /// <summary>
    /// The account id associated with this meter reading
    /// </summary>
    public int AccountId { get; set; }
    
    /// <summary>
    /// The date the reading was taken 
    /// </summary>
    public DateTime MeterReadingDateTime { get; set; }
    
    /// <summary>
    /// The meter reading value
    /// </summary>
    public int MeterReadValue { get; set; }
}