using System.Globalization;
using CsvHelper;
using EnsekTest.Database;
using EnsekTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace EnsekTest.Controllers
{
    /// <summary>
    /// Methods for working with meter reading data
    /// </summary>
    [Route("api/meter-reading-uploads")]
    [ApiController]
    public class MeterReadingsController : ControllerBase
    {
        private const int MaxReadingValue = 99999;
        private const string AccountId = "AccountId";
        private const string MeterReadingDateTime = "MeterReadingDateTime";
        private const string MeterReadValue = "MeterReadValue";
        private readonly IEnsekDbContext _dbContext;

        /// <inheritdoc />
        public MeterReadingsController(IEnsekDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Get all meter readings stored in the database
        /// </summary>
        /// <returns>Array of stored meter readings</returns>
        // GET: api/Account
        [HttpGet]
        public  ActionResult<IEnumerable<MeterReading>> Get()
        {
            return _dbContext.MeterReadings;
        }

        /// <summary>
        /// Accepts a csv of meter reading data in the format
        /// AccountId,MeterReadingDateTime,MeterReadValue
        /// Adds all valid readings to the database
        /// </summary>
        /// <param name="file">The csv file</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        // POST: api/Account
        [HttpPost]
        public async Task<ActionResult<MeterReadingUploadResults>> Post([FromForm] IFormFile file)
        {
            var result = new MeterReadingUploadResults();
            var recordsToAdd = new List<MeterReading>();

            using (var reader = new StreamReader(file.OpenReadStream()))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                // Read the CSV 
                await csv.ReadAsync();
                csv.ReadHeader();
                ActionResult? headerTestResult = ValidateCsvHeader(csv);
                if (headerTestResult != null)
                    return headerTestResult;
                
                while (await csv.ReadAsync())
                {
                    // create result record
                    var recordResult = new MeterReadingUploadResult();
                    var databaseRecord = new MeterReading();

                    bool success;

                    // Get the account number 
                    (int accountId, success) = GetCsvValue<int>(csv, AccountId, recordResult, result);
                    if (success)
                    {
                        if (!_dbContext.CustomerAccountExists(accountId))
                        {
                            recordResult.Success = false;
                            recordResult.Error = $"Account {accountId} does not exist";
                            result.Results.Add(recordResult);
                            continue;
                        }

                        databaseRecord.AccountId = accountId;
                    }
                    else
                    {
                        continue;
                    }

                    // Get the reading date
                    (string meterReadingText, success) =
                        GetCsvValue<string>(csv, MeterReadingDateTime, recordResult, result);
                    if (success)
                    {
                        databaseRecord.MeterReadingDateTime = DateTime.Parse(meterReadingText);
                    }
                    else
                    {
                        continue;
                    }

                    // Get the reading
                    (string meterReadingValue, success) = GetCsvValue<string>(csv, MeterReadValue, recordResult, result);
                    if (success)
                    {
                        try
                        {
                            int readingInt = int.Parse(meterReadingValue, NumberStyles.Integer);
                            if (readingInt is < 0 or > MaxReadingValue)
                                throw new Exception("Reading is not a valid range");
                            databaseRecord.MeterReadValue = readingInt;
                        }
                        catch (Exception)
                        {
                            recordResult.Success = false;
                            recordResult.Error = $"MeterReadValue is not in a valid format";
                            result.Results.Add(recordResult);
                            continue;
                        }
                        
                    }
                    else
                    {
                        continue;
                    }

                    // Check this isn't a duplicate reading
                    if (CanAddReading(recordsToAdd, databaseRecord))
                    {
                        recordsToAdd.Add(databaseRecord);
                    }
                    else
                    {
                        recordResult.Success = false;
                        recordResult.Error = $"Newer reading already exists or reading is lower than previous reading";
                        result.Results.Add(recordResult);
                        continue;
                    }

                    result.Results.Add(recordResult);
                    result.SuccessfulUploads++;
                }

                // Save to the database
                _dbContext.MeterReadings.AddRange(recordsToAdd);
                await _dbContext.Instance.SaveChangesAsync();
            }

            return result;
        }

        private ActionResult? ValidateCsvHeader(CsvReader csv)
        {
            if (!csv.HeaderRecord.Contains(AccountId))
                return BadRequest($"CSV does not contain column named {AccountId}");
            if (!csv.HeaderRecord.Contains(MeterReadingDateTime))
                return BadRequest($"CSV does not contain column named {MeterReadingDateTime}");
            if (!csv.HeaderRecord.Contains(MeterReadValue))
                return BadRequest($"CSV does not contain column named {MeterReadValue}");
            
            return null;
        }

        /// <summary>
        /// Helper method to read a key from the csv file
        /// </summary>
        /// <param name="csv">The csv file</param>
        /// <param name="csvColumnId">The column we're trying to read</param>
        /// <param name="recordResult">The result object for this row</param>
        /// <param name="result">The object containing all our result rows</param>
        /// <typeparam name="T">The expected type for the data in this column</typeparam>
        /// <returns>A tuple of the csv data cast to the correct type and a bool showing whether the read was a success or not</returns>
        private (T value, bool result) GetCsvValue<T>(CsvReader csv, string csvColumnId,
            MeterReadingUploadResult recordResult, MeterReadingUploadResults result)
        {
            var returnValue = default(T);
            bool success = true;
            try
            {
                returnValue = csv.GetField<T>(csvColumnId);
            }
            catch (Exception)
            {
                recordResult.Success = false;
                recordResult.Error = $"{csvColumnId} is not in a valid format";
                result.Results.Add(recordResult);
                success = false;
            }

            return (returnValue, success)!;
        }

        /// <summary>
        /// Checks to see if we are adding a duplicate reading or a reading with an older date stamp or a newer date but
        /// a smaller meter reading value.
        /// Because we are queueing readings to add to the database we 
        /// </summary>
        /// <param name="currentReadings">An array of readings we've already queued for add to the database</param>
        /// <param name="newReading">The new reading we want to add</param>
        /// <returns>True if the reading can be added or false otherwise</returns>
        private bool CanAddReading(IEnumerable<MeterReading> currentReadings, MeterReading newReading)
        {
            // Get most recent reading from the local cache for this account
            MeterReading? latestReading = currentReadings.Where(reading => reading.AccountId == newReading.AccountId)
                .OrderByDescending(date => date.MeterReadingDateTime)
                .FirstOrDefault();
            // No accounts cached yet so check the database
            if (latestReading == null)
                return _dbContext.CanAddReading(newReading);
            // If checking cache passes with no more recent reads then check database
            return newReading.MeterReadingDateTime > latestReading.MeterReadingDateTime
                   && newReading.MeterReadValue >= latestReading.MeterReadValue &&
                   _dbContext.CanAddReading(newReading);
        }
    }
}