using EnsekTest.Database;
using EnsekTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EnsekTest.Controllers
{
    /// <summary>
    /// Methods for working with customer accounts
    /// </summary>
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IEnsekDbContext _dbContext;

        /// <inheritdoc />
        public AccountController(IEnsekDbContext dbContext)
        {
            _dbContext = dbContext;
            
        }

        /// <summary>
        /// Get all customer accounts
        /// </summary>
        /// <returns>A list of all customer accounts</returns>
        // GET: api/Account
        [HttpGet]
        public  ActionResult<IEnumerable<CustomerAccount>> Get()
        {
            return _dbContext.CustomerAccounts;
        }

        /// <summary>
        /// Request a specific customer account
        /// </summary>
        /// <param name="id">The account Id</param>
        /// <returns>The customer account requested</returns>
        // GET: api/Account/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<CustomerAccount>> Get(int id)
        {
            CustomerAccount? account = await _dbContext.CustomerAccounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            return account;
        }

        /// <summary>
        /// Create a new customer account
        /// </summary>
        /// <param name="newAccount">The customer account added</param>
        /// <returns></returns>
        // POST: api/Account
        [HttpPost]
        public async Task<ActionResult<CustomerAccount>> Post([FromBody] CustomerAccount newAccount)
        {
            _dbContext.CustomerAccounts.Add(newAccount);
            await _dbContext.Instance.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = newAccount.AccountId }, newAccount);
        }

        /// <summary>
        /// Replace a customer account with the data passed in
        /// </summary>
        /// <param name="id">The customer account id</param>
        /// <param name="updatedAccount">The new data for the account</param>
        /// <returns></returns>
        // PUT: api/Account/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CustomerAccount updatedAccount)
        {
            if (id != updatedAccount.AccountId)
            {
                return BadRequest();
            }

            _dbContext.Instance.Entry(updatedAccount).State = EntityState.Modified;

            try
            {
                await _dbContext.Instance.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_dbContext.CustomerAccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Delete an account
        /// </summary>
        /// <param name="id">The account id to remove</param>
        /// <returns></returns>
        // DELETE: api/Account/5
        [HttpDelete("{id}")]
        public async Task<IActionResult>  Delete(int id)
        {
            var todoItem = await _dbContext.CustomerAccounts.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _dbContext.CustomerAccounts.Remove(todoItem);
            await _dbContext.Instance.SaveChangesAsync();

            return NoContent();
        }
    }
}
