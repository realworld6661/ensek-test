using EnsekTest.Controllers;
using EnsekTest.Database;
using EnsekTest.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace EnsekTest.Tests;

public class AccountControllerTests
{
    private Mock<IEnsekDbContext> _mockDb = null!;
    private Mock<DbSet<CustomerAccount>> _mockSet = null!;

    [SetUp]
    public void SetupDbContext()
    {
        _mockSet = new Mock<DbSet<CustomerAccount>>();
        _mockDb = new Mock<IEnsekDbContext>();
        var dbContext = new Mock<DbContext>();
        dbContext.Setup(c => c.SaveChanges()).Verifiable();
        dbContext.Setup(c => c.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();
        _mockDb.Setup(c => c.Instance).Returns(dbContext.Object);
        _mockDb.Setup(m => m.CustomerAccounts).Returns(_mockSet.Object);
    }

    [Test]
    public async Task TestAddAccount()
    {
        var controller = new AccountController(_mockDb.Object);
        await controller.Post(new CustomerAccount() { AccountId = 123, FirstName = "Test", LastName = "User" });
        
        _mockSet.Verify(m=>m.Add(It.IsAny<CustomerAccount>()), Times.Once());
        _mockDb.Verify(m=>m.Instance.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
    }
    
}