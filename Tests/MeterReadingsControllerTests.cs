using EnsekTest.Controllers;
using EnsekTest.Database;
using EnsekTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using NUnit.Framework;

namespace EnsekTest.Tests;

[TestFixture]
public class MeterReadingsControllerTests
{
    private Mock<IEnsekDbContext> _mockDb = null!;
    private Mock<DbSet<CustomerAccount>> _mockCustomerSet = null!;
    private Mock<DbSet<MeterReading>> _mockReadingSet = null!;

    [SetUp]
    public void SetupDbContext()
    {
        var sampleUsers = new List<CustomerAccount>()
        {
            new CustomerAccount
            {
                AccountId = 2344,
                FirstName = "Tommy",
                LastName = "Test"
            },
            new CustomerAccount
            {
                AccountId = 2233,
                FirstName = "Barry",
                LastName = "Test"
            },
            new CustomerAccount
            {
                AccountId = 8766,
                FirstName = "Sally",
                LastName = "Test"
            },
        };

        _mockCustomerSet = sampleUsers.AsQueryable().BuildMockDbSet();
        _mockReadingSet = new Mock<DbSet<MeterReading>>();
        _mockDb = new Mock<IEnsekDbContext>();
        var dbContext = new Mock<DbContext>();
        dbContext.Setup(c => c.SaveChanges()).Verifiable();
        dbContext.Setup(c => c.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();
        _mockDb.Setup(c => c.Instance).Returns(dbContext.Object);
        _mockDb.Setup(m => m.CustomerAccounts).Returns(_mockCustomerSet.Object);
        _mockDb.Setup(m => m.MeterReadings).Returns(_mockReadingSet.Object);
        _mockDb.Setup(m => m.CanAddReading(It.IsAny<MeterReading>())).Returns(true);
        _mockDb.Setup(m => m.CustomerAccountExists(It.IsAny<int>())).Returns((int accountId) =>
        {
            return _mockCustomerSet.Object.Any(account => account.AccountId == accountId);
        });
    }

    [Test]
    public async Task PostCsvInvalidFile()
    {
        //Arrange
        var fileMock = new Mock<IFormFile>();
        //Setup mock file using a memory stream
        var content = "Invalid csv file";
        var fileName = "test.csv";
        var ms = new MemoryStream();
        var writer = new StreamWriter(ms);
        writer.Write(content);
        writer.Flush();
        ms.Position = 0;
        fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
        fileMock.Setup(_ => _.FileName).Returns(fileName);
        fileMock.Setup(_ => _.Length).Returns(ms.Length);

        var controller = new MeterReadingsController(_mockDb.Object);
        var file = fileMock.Object;

        //Act
        var result = await controller.Post(file);

        //Assert
        Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
    }

    [Test]
    public async Task PostCsvValidFile()
    {
        //Arrange
        var fileMock = new Mock<IFormFile>();
        //Setup mock file using a memory stream
        var fileName = "test.csv";
        var ms = new MemoryStream();
        var writer = new StreamWriter(ms);
        writer.WriteLine("AccountId,MeterReadingDateTime,MeterReadValue");
        writer.WriteLine("2344,22/04/2019 09:24,01002");
        writer.WriteLine("2233,22/04/2019 12:25,00323");
        writer.Flush();
        ms.Position = 0;
        fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
        fileMock.Setup(_ => _.FileName).Returns(fileName);
        fileMock.Setup(_ => _.Length).Returns(ms.Length);

        var controller = new MeterReadingsController(_mockDb.Object);
        var file = fileMock.Object;

        //Act
        var result = await controller.Post(file);

        //Assert
        Assert.IsInstanceOf<ActionResult<MeterReadingUploadResults>>(result);
        Assert.NotNull(result.Value);
        Assert.AreEqual(2, result.Value.SuccessfulUploads);
    }
    
    [Test]
    public async Task PostCsvPartialValidFile()
    {
        //Arrange
        var fileMock = new Mock<IFormFile>();
        //Setup mock file using a memory stream
        var fileName = "test.csv";
        var ms = new MemoryStream();
        var writer = new StreamWriter(ms);
        writer.WriteLine("AccountId,MeterReadingDateTime,MeterReadValue");
        writer.WriteLine("2344,22/04/2019 09:24,01002");
        writer.WriteLine("0,22/04/2019 12:25,00323");
        writer.WriteLine("2233,22/04/2019 12:25,00323");
        writer.WriteLine("2233,22/04/2019 12:25,bobbins");
        writer.Flush();
        ms.Position = 0;
        fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
        fileMock.Setup(_ => _.FileName).Returns(fileName);
        fileMock.Setup(_ => _.Length).Returns(ms.Length);

        var controller = new MeterReadingsController(_mockDb.Object);
        var file = fileMock.Object;

        //Act
        var result = await controller.Post(file);

        //Assert
        Assert.IsInstanceOf<ActionResult<MeterReadingUploadResults>>(result);
        Assert.NotNull(result.Value);
        Assert.AreEqual(2, result.Value.SuccessfulUploads);
    }
}