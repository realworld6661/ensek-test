window.addEventListener('DOMContentLoaded', (event) => {
    $('#uploadReadingsForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            avatar: {
                validators: {
                    file: {
                        extension: 'csv',
                        type: 'text/csv',
                        message: 'The selected file is not valid'
                    }
                }
            }
        }
    });    
});

function showAccounts() {
    if ($('#accountList').is('.collapse:not(.show)')) {
        // If expanding then run the API request
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                const accounts = JSON.parse(xmlHttp.responseText);
                // Add a row for each account
                for (const account of accounts) {
                    $('#accountContainer').append(`<div class="row">
                                                        <div class="col">
                                                            ${account.accountId}
                                                        </div>
                                                        <div class="col">
                                                            ${account.firstName}
                                                        </div>
                                                        <div class="col">
                                                            ${account.lastName}
                                                        </div>
                                                    </div>`);
                }
                // Call expand logic
                const collapseElementList = [].slice.call(document.querySelectorAll('#accountList'));
                const collapseList = collapseElementList.map(function (collapseEl) {
                    return new bootstrap.Collapse(collapseEl)
                });
            }
        }
        xmlHttp.open("GET", "https://localhost:7052/api/account", true); // true for asynchronous 
        xmlHttp.send(null);
    } else {
        // Remove any existing rows
        $("#accountContainer").find(".row:gt(0)").remove();

        // Collapse container
        var collapseElementList = [].slice.call(document.querySelectorAll('#accountList'))
        const collapseList = collapseElementList.map(function (collapseEl) {
            return new bootstrap.Collapse(collapseEl)
        });
    }
}

function showReadings() {
    if ($('#readingList').is('.collapse:not(.show)')) {
        // If expanding then run the API request
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                const readings = JSON.parse(xmlHttp.responseText);
                // Add a row for each account
                for (const reading of readings) {
                    $('#readingContainer').append(`<div class="row">
                                                        <div class="col">
                                                            ${reading.accountId}
                                                        </div>
                                                        <div class="col">
                                                            ${reading.meterReadingDateTime}
                                                        </div>
                                                        <div class="col">
                                                            ${reading.meterReadValue}
                                                        </div>
                                                    </div>`);
                }
                // Call expand logic
                const collapseElementList = [].slice.call(document.querySelectorAll('#readingList'));
                const collapseList = collapseElementList.map(function (collapseEl) {
                    return new bootstrap.Collapse(collapseEl)
                });
            }
        }
        xmlHttp.open("GET", "https://localhost:7052/api/meter-reading-uploads", true); // true for asynchronous 
        xmlHttp.send(null);
    } else {
        // Remove any existing rows
        $("#readingContainer").find(".row:gt(0)").remove();

        // Collapse container
        var collapseElementList = [].slice.call(document.querySelectorAll('#readingList'))
        const collapseList = collapseElementList.map(function (collapseEl) {
            return new bootstrap.Collapse(collapseEl)
        });
    }
}

async function uploadReadings() {
    $('#uploadError').hide();
    const input = $('input[type="file"]');
    const formData = new FormData();
    formData.append('file', input[0].files[0]);
    try {
        const response = await fetch('api/meter-reading-uploads', {
            method: 'POST',
            headers: {
                'documentType': 'text/csv'
            },
            body: formData
        });
        if ( !response.ok )
        {
            throw new Error(await response.text());
        }
        let text = await response.text();        
        var results = JSON.parse(text);
        $('#readingsPostResults').show().text(`Successful uploads: ${results.successfulUploads}`);    
    }
    catch (e) {
        $('#uploadError').show().text(e);
    }
    
}