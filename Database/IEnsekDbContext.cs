using EnsekTest.Models;
using Microsoft.EntityFrameworkCore;

namespace EnsekTest.Database;

public interface IEnsekDbContext
{
    DbContext Instance { get; }
    DbSet<CustomerAccount> CustomerAccounts { get; set; }
    DbSet<MeterReading> MeterReadings { get; set; }
    
    /// <summary>
    /// Checks if the accounts list contains an account with the specified accountId
    /// </summary>
    /// <param name="accountId">The account id to search for</param>
    /// <returns>True if the account does not already exist</returns>
    bool CustomerAccountExists(int accountId);
    
    /// <summary>
    /// Checks if this reading is newer than one already existing for this account
    /// </summary>
    /// <param name="newReading">The reading we want to add to the database</param>
    /// <returns>True if this is a valid reading to add</returns>
    bool CanAddReading(MeterReading newReading);
}