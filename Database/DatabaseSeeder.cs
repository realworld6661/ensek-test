using System.Globalization;
using System.Text;
using CsvHelper;
using EnsekTest.Models;
using Extensions.Hosting.AsyncInitialization;

namespace EnsekTest.Database;

/// <summary>
/// Called on app startup to seed the database with list of user accounts
/// </summary>
public class DatabaseSeeder : IAsyncInitializer
{
    private readonly IEnsekDbContext _context;

    public DatabaseSeeder(IEnsekDbContext context)
    {
        _context = context;
    }

    /// <inheritdoc />
    public async Task InitializeAsync()
    {
        _context.Instance.Database.EnsureCreated();

        // Seed the local database for sample project
        try
        {
            using (var reader = new StreamReader("Data/Test_Accounts.csv", Encoding.Default))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                List<CustomerAccount> records = csv.GetRecords<CustomerAccount>().ToList();
                await _context.CustomerAccounts.AddRangeAsync(records);
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }

        _context.Instance.SaveChanges();
    }
}