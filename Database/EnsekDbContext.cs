using EnsekTest.Models;
using Microsoft.EntityFrameworkCore;

namespace EnsekTest.Database;

/// <summary>
/// Application database context
/// </summary>
public class EnsekDbContext : DbContext, IEnsekDbContext
{
    public DbContext Instance => this;

    public DbSet<CustomerAccount> CustomerAccounts { get; set; } = null!;
    public DbSet<MeterReading> MeterReadings { get; set; } = null!;

    public EnsekDbContext(DbContextOptions<EnsekDbContext> options) : base(options)
    {
    }
    
    /// <summary>
    /// Checks if the accounts list contains an account with the specified accountId
    /// </summary>
    /// <param name="accountId">The account id to search for</param>
    /// <returns>True if the account does not already exist</returns>
    public bool CustomerAccountExists(int accountId)
    {
        return CustomerAccounts.Any(account => account.AccountId == accountId);
    }

    /// <summary>
    /// Checks if this reading is newer than one already existing for this account
    /// </summary>
    /// <param name="newReading">The reading we want to add to the database</param>
    /// <returns>True if this is a valid reading to add</returns>
    public bool CanAddReading(MeterReading newReading)
    {
        // Get the most recent meter reading for this account id
        MeterReading? latestReading = MeterReadings.Where(reading => reading.AccountId == newReading.AccountId)
                                                .OrderByDescending(date=>date.MeterReadingDateTime)
                                                .FirstOrDefault();
        
        // None exist so its ok to add reading
        if (latestReading == null)
            return true;
        
        // Check if our new reading is more recent and that the reading is not less than the most recent
        return newReading.MeterReadingDateTime > latestReading.MeterReadingDateTime
            && newReading.MeterReadValue >= latestReading.MeterReadValue;
    }
}