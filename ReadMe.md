## Getting Started
1. Build and start the server 
2. Navigate to https://localhost:7052/index.html
3. Verify the WebAPI has started and that database has been seeded by clicking "Show/Hide Account List". This should return all the accounts from the Test_Accounts.csv file
4. Upload the Meter_Reading.csv file 
5. The number of successfully uploaded records should be displayed.
6. Click the "Show/Hide Uploaded Readings" button to show all the readings stored in the database

## Dependencies
* **Microsoft.EntityFrameworkCore.InMemory** - For the purposes of this small project, I am using the in memory database context. In a real system this would be a hosted database
* **CsvHelper** - This is required for seeding the database with sample data and parsing the `POST` data from the `meter-reading-uploads` endpoint 
* **Extensions.Hosting.AsyncInitialization** - extension for async initialisation where I can run the database seeding logic before the service starts
* **Moq** - For mocking dependencies in unit tests
* **MockQueryable** - For mocking EF database requests

## Notes
* I have assumed that newer readings should always be >= the most recent reading on an account. Normally I would clarify this but I wasn't able to do so during this test.
* The web interface to the API is a very rough and dirty frontend thrown together with Bootstrap and jQuery to demonstrate the functionality of the API and not to demonstrate the best way to develop frontend code.
* The accounts API exposes CRUD methods for customer accounts. This in particular would require some authentication in a production system but is outside the scope of this test.
* There is far more that could be covered by the unit tests but as they were not part of the test spec I covered some basics.
* The `CanAddReading` method in `MeterReadingsController` could make a lot of database requests, especially if the uploaded csv contained thousands of rows. Without knowing the likely use case of this in a real situation (perhaps the csv's would never have more than a few dozen rows or the end point is called very infrequently) that may be acceptable.  